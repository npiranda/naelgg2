var express = require('express');
var cors = require('cors');
const axios = require('axios');
const {response} = require("express");

var app = express();

app.use(cors());

const API_KEY = "RGAPI-4abdf226-842b-426f-acb7-f3ae79ac68ee";

function getPlayerPUUID(playerName) {
    return axios.get("https://euw1.api.riotgames.com" + "/lol/summoner/v4/summoners/by-name/" + playerName + "?api_key=" + API_KEY)
        .then(response => {
            return response.data.puuid;
        }).catch(err => err);
}

// GET past five games
// localhost:4000/past5Games
app.get('/past5Games', async (req, res) => {
    const playerName = req.query.username;
    //PUUID
    const PUUID = await getPlayerPUUID(playerName);
    console.log(PUUID);
    const API_CALL = "https://europe.api.riotgames.com/lol/match/v5/matches/by-puuid/" + PUUID + "/ids?start=0&count=5&api_key=" + API_KEY;

    //get API_CALL
    const gameIDs = await axios.get(API_CALL)
        .then(response => response.data)
        .catch(err => err);


    var matchDataArray = [];
    for (let i = 0; i < gameIDs.length; i++) {
        const matchID = gameIDs[i];
        console.log(matchID);
        const matchData = await axios.get("https://europe.api.riotgames.com/lol/match/v5/matches/" + matchID + "?api_key=" + API_KEY)
            .then(response => response.data)
            .catch(err => err);
        matchDataArray.push(matchData);
    }

    res.json(matchDataArray);

});

app.listen(4000, function () {
    console.log("Server started on port 4000");
}) // localhost:4000



