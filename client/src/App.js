import { useState} from "react";
import axios from "axios";
import './App.css';
import Header from './components/Header.js'

function App() {
    const [gameList, setGameList] = useState([]);
    const gameState = {
        gameList, setGameList
    };


    function championBackground(data) {
        return {
            backgroundImage: 'url(http://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + data.championName +'_0.jpg)',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundOpacity: 0.5
        };
    }

    return (
    <div className="App p-9">
        <Header gameState={gameState}/>
        {gameList.length !== 0 ?
            <>
               <p>We have data!</p>
                {
                    gameList.map((gameData, index) =>
                    <>
                        <div className="px-4 py-5 sm:px-6 ">
                            <h2 className="text-lg font-medium leading-6 text-gray-900">Game {index + 1}</h2>
                            <p className="mt-1 max-w-2xl text-sm text-gray-500">Personal details and application.</p>
                        </div>
                            <div className="border-t border-gray-200 rounded-2xl">
                                <div className="grid grid-cols-5 gap-4 p-2">
                                    {gameData.info.participants.map((data, participantIndex) =>
                                        <div>
                                            <div style={championBackground(data)} className="overflow-hidden bg-white shadow sm:rounded-lg hover:shadow-2xl transition duration-300 ease-in-out hover:shadow-2xl transition duration-300 ease-in-out text-white text-shadow">
                                                <p className={"mt-32 text-3xl"}>{data.summonerName}</p>
                                                <p>{data.kills} / {data.deaths} / {data.assists}</p>
                                                <div className={"itemsList pt-4"}>
                                                    {data.item0 !== 0 && <img className={"p-1 items"} src={"https://ddragon.leagueoflegends.com/cdn/13.3.1/img/item/"+data.item0+".png"}/>}
                                                    {data.item1 !== 0 && <img className={"p-1 items"} src={"https://ddragon.leagueoflegends.com/cdn/13.3.1/img/item/"+data.item1+".png"}/>}
                                                    {data.item2 !== 0 && <img className={"p-1 items"} src={"https://ddragon.leagueoflegends.com/cdn/13.3.1/img/item/"+data.item2+".png"}/>}
                                                    {data.item3 !== 0 && <img className={"p-1 items"} src={"https://ddragon.leagueoflegends.com/cdn/13.3.1/img/item/"+data.item3+".png"}/>}
                                                    {data.item4 !== 0 && <img className={"p-1 items"} src={"https://ddragon.leagueoflegends.com/cdn/13.3.1/img/item/"+data.item4+".png"}/>}
                                                    {data.item5 !== 0 && <img className={"p-1 items"} src={"https://ddragon.leagueoflegends.com/cdn/13.3.1/img/item/"+data.item5+".png"}/>}
                                                </div>
                                                <div className={"pt-4 pb-4"}>
                                                    {data.firstBloodKill && <span className="bg-blue-100 text-blue-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300">First blood</span>}
                                                    {(data.doubleKills !== 0 && data.tripleKills === 0) && <span className="bg-green-100 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-900 dark:text-green-300">Double kill</span>}
                                                    {(data.tripleKills !== 0 && data.quadraKills === 0) && <span className="bg-green-100 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-900 dark:text-green-300">Triple kill</span>}
                                                    {(data.quadraKills !== 0 && data.pentaKills === 0) && <span className="bg-green-100 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-900 dark:text-green-300">Quadrakill</span>}
                                                    {data.pentaKills !== 0 && <span className="bg-green-100 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-900 dark:text-green-300">Pentakill</span>}
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </div>
                            </div>
                    </>
                    )
                }
            </>
            :
            <>
                <p>We have NO data!</p>
            </>
        }
    </div>
  );
}

export default App;
