import {useState} from "react";
import axios from "axios";

export default function Header({gameState}) {
    const [searchText, setSearchText] = useState("");
    const {
        gameList,
        setGameList
    } = gameState

    function getPlayerGames(event) {
        axios.get("http://localhost:4000/past5Games", { params: { username: searchText}})
            .then(function (response) {
                setGameList(response.data)
            }).catch(function (error) {
            console.log(error);
        })
    }

    return(
        <div>
            <h2 className="text-3xl font-bold underline">Welcome to our proxy server app!</h2>
            <input type="text" onChange={e => setSearchText(e.target.value)}/>
            <button onClick={getPlayerGames}>Get the past 5 games from your player</button>
        </div>
    )
}
